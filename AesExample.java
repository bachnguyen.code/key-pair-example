import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class AesExample {

    /**
     * AES (Advanced Encryption Standard) là một thuật toán “mã hóa khối” (block cipher).
     * AES trở thành một trong những thuật toán mã hóa phổ biến nhất sử dụng khóa mã đối xứng để mã hóa và giải mã
     * (một số được giữ bí mật dùng cho quy trình mở rộng khóa nhằm tạo ra một tập các khóa vòng).
     *
     * AES là một thuật toán mã hóa khối đối xứng với độ dài khóa là 128 bít(một chữ số nhị phân có giá trị 0 hoặc 1)
     * 192 bit và 256 bíi tương ứng dọi là AES-128, AES-192 và AES-256. AES-128 sử dụng 10 vòng (round), AES-192 sử dụng 12 vòng và AES-256 sử dụng 14 vòng.
     *
     * Đặc điêm kỹ thuật
     * Bản rõ (Plaintext): Dạng ban đầu của thông báo
     * Bản mã (Ciphertext): Dạng mã của bản rõ ban đầu
     * Khóa (Key): thông tin tham số dùng để mã hóa
     * Mã hóa (Encryption): Quá trình biến đổi thông tin từ dạng bản rõ sang bản mã bằng khóa hoặc không cần khóa
     * Giải mã (Decryption): Quá trình ngược lại biến đổi thông tin từ dạng bản mã sang bản rõ
     *
     * AES được thực hiện bởi các hàm theo thứ tự sau:
     * Trộn từng byte (SubBytes),
     * trộn từng hàng (ShiftRows),
     * trộn từng cột (MixColumns)
     * và mã hóa (AddRoundKey).
     * Trong đó SubBytes, ShiftRows, MixColumns có nhiệm vụ làm cho mối quan hệ giữa bản rõ và bản mã bị che khuất (phương thức "mập mờ").
     * AddRoundKey sử dụng key mã hóa để mã hóa dữ liệu đầu vào bằng việc phân tán những kiểu mẫu của bản rõ sang bản mã (phương thức "khuếch tán")
     *
     * Chi tiết: https://vi.wikipedia.org/wiki/Advanced_Encryption_Standard
     */
    public static void demo() throws NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        System.out.println("=================================== Start aes example ===================================");

        String originalString = "demo AES";

        // https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
        // https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html
        // tạo khóa bí mật với key có dộ dài 16 ký tự, sử dụng thuật toán AES
        SecretKeySpec secretKeySpec = new SecretKeySpec("1234567891234567".getBytes(), "AES");

        // Cipher cung cấp chức năng của cryptographic cipher để mã hóa và giải mã.
        // Nó tạo thành cốt lõi của Java Cryptographic Extension (JCE) framework.
        Cipher cipher = Cipher.getInstance("AES");

        // khởi tạo chế độ mã hóa dựa vào secretKeySpec đã được tạo ở trên
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        // từ chuỗi ban đầu (originalString), sử dụng cipher để mã hóa
        String encryptedString = Base64.getEncoder().encodeToString(cipher.doFinal(originalString.getBytes("UTF-8")));

        // khởi tạo chế độ giải mã dựa vào secretKeySpec đã được tạo ở trên
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        // từ chuỗi đã được mã hóa (encryptedString), tiến hành gia mã
        String decryptedString = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedString)));

        // in ra màn hình kết quả
        System.out.println("originalString:  " + originalString);
        System.out.println("encryptedString: " + encryptedString);
        System.out.println("decryptedString: " + decryptedString);

        System.out.println("=================================== end aes example ===================================");
    }
}
