import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class DesExample {

    /**
     * Data Encryption Standard (DES) là một thuật toán mã hóa dữ liệu.
     * Là dạng mã hóa khối, kích thước khối vào 64 bit
     * Khóa 64 bít, trong đó thực sử dụng 56 bít, 8 bít dùng cho kiểm tra chẵn lẻ
     * DES sử dụng chung một giải thuật cho mã hóa và giải mã.
     *
     * Hiện nay DES không được coi là an toàn do:
     * Không gian khóa nhỏ (khóa 64 bít, trong đó thực sử dụng 56 bít)
     * Tốc độ tính toán của các hệ thống máy tính ngày càng nhanh.
     *
     * DES là loại mã hóa đối xứng (mã hóa khóa bí mật), sử dụng một khóa bí mật duy nhất cho cả quá trình mã hóa và giải mã
     */
    public static void demo() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        System.out.println("=================================== Start des example ===================================");

        String original = "demo DES";

        // https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#Cipher
        // https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html
        // tạo khóa bí mật với key có dộ dài 8 ký tự, sử dụng thuật toán DES
        SecretKeySpec skeySpec = new SecretKeySpec("12345678".getBytes(), "DES");

        // Cipher cung cấp chức năng của cryptographic cipher để mã hóa và giải mã.
        // Nó tạo thành cốt lõi của Java Cryptographic Extension (JCE) framework.
        Cipher cipher = Cipher.getInstance("DES");

        // khởi tạo chế độ mã hóa dựa vào skeySpec đã được tạo ở trên
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        // từ chuỗi ban đầu (original), sử dụng cipher để mã hóa, trả ra dạng byte
        byte[] byteEncrypted = cipher.doFinal(original.getBytes());
        // từ dạng byte chuyển sang string
        String encrypted =  Base64.getEncoder().encodeToString(byteEncrypted);

        // khởi tạo chế độ giải mã dựa vào skeySpec đã được tạo ở trên
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        // từ byte đã mã hóa (byteEncrypted), sử dụng cipher để giải hóa, trả ra dạng byte
        byte[] byteDecrypted = cipher.doFinal(byteEncrypted);
        // từ dạng byte chuyển sang string
        String decrypted = new String(byteDecrypted);

        // in ra màn hình kết quả
        System.out.println("original  text: " + original);
        System.out.println("encrypted text: " + encrypted);
        System.out.println("decrypted text: " + decrypted);

        System.out.println("=================================== end des example ===================================");
    }
}
