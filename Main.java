import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, IOException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
//        DesExample.demo();
        AesExample.demo();
//        IdeaExample();
    }

    public static void IdeaExample() throws IOException {
        // đọc file plaintext.txt và mã hóa vào file ciphertext.dat
        IdeaFileEncryption.cryptFile(
                "plaintext.txt",
                "ciphertext.dat",
                "sesame",
                true,
                IdeaFileEncryption.Mode.ECB
        );
        // đọc file ciphertext.dat và giải mã vào file decrypted.txt
        IdeaFileEncryption.cryptFile(
                "ciphertext.dat",
                "decrypted.txt",
                "sesame",
                false,
                IdeaFileEncryption.Mode.ECB
        );
    }
}
