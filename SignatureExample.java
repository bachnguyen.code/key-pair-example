import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class SignatureExample {

    private static final String signatureAlgorithm = "SHA256withRSA";
    private static final String keyPairAlgorithm = "RSA";
    private static final String outFileFake = "example-fake";
    private static final String outFile = "example";
    private static final Base64.Encoder encoder = Base64.getEncoder();
    private static final Base64.Decoder decoder = Base64.getDecoder();

    public static void demo() throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException, IOException {
        // Tạo cặp khóa bí mật, khóa công khai thật
        generateKeyPair(outFile);
        // Tạo cặp khóa bí mật, khóa công khai giả
        generateKeyPair(outFileFake);

        // Từ cặp khóa đã tạo, tạo signSignatureFake dùng để ký chữ ký kĩ thụât số
        Signature signSignatureFake = provideSignSignature(getFormattedKey(outFileFake + ".key"));
        // Từ cặp khóa đã tạo, tạo signSignature dùng để ký chữ ký kĩ thụât số
        Signature signSignature = provideSignSignature(getFormattedKey(outFile + ".key"));
        // Từ cặp khóa đã tạo, tạo verifySignature dùng để xác thực chữ ký kĩ thụât số
        Signature verifySignature = provideVerifySignature(getFormattedKey(outFile + ".pub"));

        System.out.println("=============START=============");
        // Cung cấp dữ liệu mẫu
        byte[] dataSign = "this_is_data_sign".getBytes(StandardCharsets.UTF_8);
        // Dùng dữ liệu mẫu để tạo chữ ký kĩ thuật số giả
        signSignatureFake.update(dataSign);
        // Dùng dữ liệu mẫu để tạo chữ ký kĩ thuật số thật
        signSignature.update(dataSign);
        // Dùng dữ liệu mẫu để chuẩn bị xác thực
        verifySignature.update(dataSign);

        // in ra kết quả xác thực đối với cặp key thật
        System.out.println("real key verify status: " + verifySignature.verify(signSignature.sign()));
        // in ra kết quả xác thực đối với cặp key giả
        System.out.println("fake key verify status: " + verifySignature.verify(signSignatureFake.sign()));

        System.out.println("=============END=============");
    }

    private static String getFormattedKey(String filePath) throws IOException {
        return new String(Files.readAllBytes(Paths.get(filePath)))
                .replaceAll("\n", "")
                .replaceAll("\t", "")
                .replace("-----BEGIN RSA PRIVATE KEY-----", "")
                .replace("-----END RSA PRIVATE KEY-----", "")
                .replace("-----BEGIN RSA PUBLIC KEY-----", "")
                .replace("-----END RSA PUBLIC KEY-----", "");
    }

    public static void generateKeyPair(String _outFile) throws NoSuchAlgorithmException, IOException {
        System.out.println("=============start generating key pair " + _outFile + "=============");
        KeyPairGenerator kpg = KeyPairGenerator.getInstance(keyPairAlgorithm);
        kpg.initialize(2048);
        KeyPair kp = kpg.generateKeyPair();
        Key pub = kp.getPublic();
        Key pvt = kp.getPrivate();

        Writer out = new FileWriter(_outFile + ".key");
        out.write("-----BEGIN RSA PRIVATE KEY-----\n");
        out.write(encoder.encodeToString(pvt.getEncoded()) + "\n");
        out.write("-----END RSA PRIVATE KEY-----");
        out.close();

        out = new FileWriter(_outFile + ".pub");
        out.write("-----BEGIN RSA PUBLIC KEY-----\n");
        out.write(encoder.encodeToString(pub.getEncoded()) + "\n");
        out.write("-----END RSA PUBLIC KEY-----");
        out.close();

        System.out.println("Private key: " + pvt);
        System.out.println("Public key: " + pub);

        System.out.println("=============end generating key pair " + _outFile + "=============");
    }

    public static Signature provideVerifySignature(String key) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException {
        PublicKey publicKey = KeyFactory.getInstance(keyPairAlgorithm)
                .generatePublic(new X509EncodedKeySpec(decoder.decode(key)));
        Signature signature = Signature.getInstance(signatureAlgorithm);
        signature.initVerify(publicKey);
        return signature;
    }

    public static Signature provideSignSignature(String key) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        PrivateKey privateKey = KeyFactory.getInstance(keyPairAlgorithm)
                .generatePrivate(new PKCS8EncodedKeySpec(decoder.decode(key)));
        Signature signature = Signature.getInstance(signatureAlgorithm);
        signature.initSign(privateKey);
        return signature;
    }
}
